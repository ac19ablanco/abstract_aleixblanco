
package figures_herencia;

import java.util.ArrayList;


public class Llista {
    public class List<Figura> extends ArrayList<Figura>{
    
    @Override
    public boolean add(Figura figura) {
        boolean estaAfegit = false;       
        
        if(!contains(figura)){
            super.add(figura);
            estaAfegit = true;
        }
        return estaAfegit;
    }
    
}
}
