
package figures_herencia;

public class Rectangle extends Figura{
    
    private double amplada;
    private double alçada;

    public Rectangle(String nom, double amplada, double altura) {
        super(nom);
        this.amplada = amplada;
        this.alçada = altura;
        
    }
    
    @Override
    public double perimetre(){
        return (2 * amplada) + (2 * alçada);
        
    }
    
    @Override
    public double area(){
        return amplada * alçada;
        
    }    
    public double getAmplada(){
        return amplada;
        
    }
    public void setAmplada(double amplada){
        this.amplada = amplada;
        
    }   
    public double getAlçada(){       
        return alçada;
    }
    
    public void setAlçada(double altura){      
        this.alçada = altura;
        
    }
    @Override
    public String toString() {
        return "Rectangle { " + "amplada = " + amplada + ", altura = " + alçada + '}';
    }
    
    
    
}