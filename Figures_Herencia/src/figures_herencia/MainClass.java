
package figures_herencia;

import java.util.ArrayList;
import java.util.List;

public class MainClass {

    public static void main(String[] args) {
        
        List<Figura> llista = new ArrayList<Figura>();
        
        Cercle figura1 = new Cercle("Cercle 1", 20);
        Rectangle figura2 = new Rectangle("Rectangle 1", 21, 22);
        
        llista.add(figura1);
        llista.add(figura2);
        
        Figura figura3 = new Cercle("Cercle 2",25);
        
        llista.add(figura3);
        System.out.println(llista);
        
        
    
    }
}
