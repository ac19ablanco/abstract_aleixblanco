package figures_herencia;

public class Cercle extends Figura {

    private double radi;

    public Cercle(String nom, double radi) {
        super(nom);
        this.radi = radi;

    }
    @Override
    public double perimetre() {
        return 2 * Math.PI * radi;

    }
    @Override
    public double area() {
        return Math.PI * radi * radi;
    }
    public double getRadi() {
        return radi;

    }
    public void setRadi(double radi) {
        this.radi = radi;

    }
    @Override
    public String toString() {
        return "Cercle{ " + "radi = " + radi + '}';
    }

}
