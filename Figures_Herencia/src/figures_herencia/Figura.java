
package figures_herencia;

public abstract class Figura {
    private String nom;
           
    public Figura (String nom){
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
    
    public abstract double perimetre();
    
    public abstract double area();


}
